#!/bin/sh
# vim:sw=4:ts=4:et

# from default nginx >>>
set -e

if [ -z "${NGINX_ENTRYPOINT_QUIET_LOGS:-}" ]; then
    exec 3>&1
else
    exec 3>/dev/null
fi

if [ "$1" = "nginx" -o "$1" = "nginx-debug" -o "$1" = "/usr/bin/supervisord" ]; then
    if /usr/bin/find "/docker-entrypoint.d/" -mindepth 1 -maxdepth 1 -type f -print -quit 2>/dev/null | read v; then
        echo >&3 "$0: /docker-entrypoint.d/ is not empty, will attempt to perform configuration"

        echo >&3 "$0: Looking for shell scripts in /docker-entrypoint.d/"
        find "/docker-entrypoint.d/" -follow -type f -print | sort -V | while read -r f; do
            case "$f" in
                *.sh)
                    if [ -x "$f" ]; then
                        echo >&3 "$0: Launching $f";
                        "$f"
                    else
                        # warn on shell scripts without exec bit
                        echo >&3 "$0: Ignoring $f, not executable";
                    fi
                    ;;
                *) echo >&3 "$0: Ignoring $f";;
            esac
        done

        echo >&3 "$0: Configuration complete; ready for start up"
    else
        echo >&3 "$0: No files found in /docker-entrypoint.d/, skipping configuration"
    fi
fi
# <<< from default nginx

# Map host user id with nginx user in docker container
if [ ! -z "$PUID" ]; then
  if [ -z "$PGID" ]; then
    PGID=${PUID}
  fi
  sed -i -e 's/nginx:x:[0-9]*:/nginx:x:'$PGID':/g' /etc/group
  sed -i -e 's/nginx:x:[0-9]*:[0-9]*:/nginx:x:'$PUID':'$PGID':/g' /etc/passwd
fi


if [ "$APP_ENV" = "production" ] ; then
  cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
  echo "xdebug.mode=off" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
else
  if [ -n "$XDEBUG_MODE" ] ; then
    echo "xdebug.mode=$XDEBUG_MODE" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini ;
  fi
  cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
fi


# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- /usr/bin/supervisord -n "$@"
fi


exec "$@"
