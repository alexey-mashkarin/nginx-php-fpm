
## Генерация сертификата

```shell
    openssl req -x509 -out certs/tls.crt -keyout certs/tls.key -days 365 \
    -newkey rsa:2048 -nodes -sha256 \
    -subj '/CN=*.external.k8s' -extensions EXT -config <( \
    printf "[dn]\nCN=*.external.k8s\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:*.external.k8s\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```
